(function() {
	var game = new Phaser.Game(1000, 600, Phaser.CANVAS, "game-container");
	game.state.add("Boot",boot);
	game.state.add("Preload",preload);
	game.state.add("GameMenu",gameMenu);
	game.state.add("GameOptionsMenu",gameOptionsMenu);
	game.state.add("GameCredits",gameCredits);
	game.state.add("TheGame",theGame);
	game.state.add("GameOver",gameOver);
	game.state.start("Boot");
})();