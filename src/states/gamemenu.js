var gameMenu = function(game){
	enableButtons = true;
}

gameMenu.prototype = {
  	create: function(){
		this.game.add.sprite(0, 0, "mountains-cactus");
		var gameTitle = this.game.add.sprite(300,170,"poster_mainMenuTitle");
		gameTitle.scale.set(0.6);
		gameTitle.anchor.setTo(0.5,0.5);
		this.game.add.sprite(this.game.world.width - 420, this.game.world.height - 440,"signal_menu");
		var playButton = this.game.add.button(this.game.world.width - 188, 240,"signal_start",this.playTheGame,this);
		var optionButton = this.game.add.button(this.game.world.width - 420, this.game.world.height - 230,"signal_options",this.optionsforTheGame,this);
		var creditsButton = this.game.add.button(this.game.world.width - 268, this.game.world.height - 75,"signal_credits",this.creditsforTheGame,this);
		playButton.anchor.setTo(0.5,0.5);
		optionButton.anchor.setTo(0,0);
		creditsButton.anchor.setTo(0.5,0.5);
	},
	playTheGame: function(){
		/*this.exitMenu();*/
		this.game.state.start("TheGame");
	},
	optionsforTheGame: function(){
		/*this.exitMenu();*/
		this.game.state.start("GameOptionsMenu");
	},
	creditsforTheGame: function(){
		/*this.exitMenu();*/
		this.game.state.start("GameCredits");
	}/*,
	exitMenu: function(){
		if(enableButtons){
			enableButtons = false;
		}
	}*/
}