var preload = function(game){}

preload.prototype = {
	preload: function(){
		//Create loading bar
        var loadingBar = this.add.sprite(500,this.game.world.height - 300,"loading");
       	loadingBar.anchor.setTo(0.5,0.5);
        this.load.setPreloadSprite(loadingBar);

        //Load images:
        //--Menu
		this.game.load.image("poster_mainMenuTitle","src/assets/images/poster_mainMenuTitle.png");
		this.game.load.image("signal_menu","src/assets/images/signal_menu.png");
		this.game.load.image("signal_start","src/assets/images/signal_start.png");
		this.game.load.image("signal_options","src/assets/images/signal_options.png");
		this.game.load.image("signal_credits","src/assets/images/signal_credits.png");
		this.game.load.image("mountains-cactus","src/assets/images/mountains-cactus1000x600.png");
		//--Credits
		this.game.load.image("spiraldawn_logo_circuit","src/assets/images/spiraldawn_logo.png");
		this.game.load.image("img_phaser_logo","src/assets/images/img_phaser_logo.png");
		this.game.load.image("logo_ies_copernic","src/assets/images/logo_ies_copernic.jpg");
		//--Options
		//--Final screens
		this.game.load.image("poster_gameOver","src/assets/images/poster_gameOver.png");
		this.game.load.image("poster_score","src/assets/images/poster_score.png");
		this.game.load.image("poster_main_menu","src/assets/images/poster_main_menu.png");
		this.game.load.image("poster_re_play","src/assets/images/poster_re_play.png");
		//--Game things
		this.game.load.image('clear-sky', 'src/assets/images/clear-sky.png');
	    this.game.load.image('desert-ground-border', 'src/assets/images/desert-ground-border.png');
		this.game.load.image('desert-ground', 'src/assets/images/desert-ground.png');
		this.game.load.spritesheet('buildings', 'src/assets/images/buildings.png', 370, 400);
		this.game.load.spritesheet('plataforms', 'src/assets/images/plataforms.png', 320, 8);
	    this.game.load.image('columns', 'src/assets/images/columns.png');
	    this.game.load.image('clock', 'src/assets/images/clock.png');
	    this.game.load.image('clock-base', 'src/assets/images/clock-base.png');
	    this.game.load.spritesheet('clockwise-minutes', 'src/assets/images/clockwise.png', 35, 29);
	    this.game.load.spritesheet('clockwise-hour', 'src/assets/images/clockwise.png', 26, 16);
	    //--Characters
	    this.game.load.spritesheet('cowboy', 'src/assets/images/spritesheet_enemy.png', 30, 30);
	},
  	create: function(){
		this.game.state.start("GameMenu");
	}
}