var theGame = function(game){
	platforms = null;
    borders = null;

    scoreText = null;
    score = 0;
    scoreIncrement = 100;
    scoreString = null;

    enemies = null;
    enemyFPS = 5;
    enemySpeed = 100;
    spawner = null;
    spawnTime = 1000;
    positions = [];

    hp = 3;
    hpString = null;
    hpText = null;

    //others
    clock_time =null;
}

theGame.prototype = {
  	create: function(){
        positions[0] = {x:32, y:400};
        positions[1] = {x:140, y:430};
        positions[2] = {x:240, y:400};
        positions[3] = {x:360, y:275};
        positions[4] = {x:470, y:430};
        positions[5] = {x:470, y:163};
        positions[6] = {x:680, y:275};
        positions[7] = {x:680, y:400};
        positions[8] = {x:790, y:430};
        positions[9] = {x:890, y:400};

		var ledge;
    	//Behind sprites (the musgo):
        //--Sky and buildings
    	this.game.add.sprite(0, 0, 'clear-sky');
    	this.game.add.sprite(0, this.game.world.height - 90, 'desert-ground');
    	this.game.add.sprite(10, 110, 'buildings', 0);
    	this.game.add.sprite(324, 110, 'buildings', 1);
    	this.game.add.sprite(638, 110, 'buildings', 2);

    	//--Group plataforms
    	platforms = this.game.add.group();
        platforms.enableBody = true;

    	//--Create roofs
        ledge = platforms.create(0, this.game.world.height - 90, 'desert-ground-border');
        ledge.body.immovable = true;
    	ledge = platforms.create(10, 369, 'plataforms', 0);
    	ledge.body.immovable = true;
    	ledge = platforms.create(338, 369, 'plataforms', 0);
    	ledge.body.immovable = true;
    	ledge = platforms.create(666, 369, 'plataforms', 0);
        ledge.body.immovable = true;
        ledge = platforms.create(10, 244, 'plataforms', 2);
        ledge.body.immovable = true;
        ledge = platforms.create(338, 244, 'plataforms', 2);
        ledge.body.immovable = true;
        ledge = platforms.create(666, 244, 'plataforms', 2);
        ledge.body.immovable = true;

        //--Create enemies
        enemies = this.game.add.group();

        this.createEnemy();
        spawner = this.game.time.events.loop(spawnTime, this.createEnemy, this);

        //Front sprites:
        //--Columns
        this.game.add.sprite(10, 377, 'columns');
        this.game.add.sprite(116, 377, 'columns');
        this.game.add.sprite(209, 377, 'columns');
        this.game.add.sprite(315, 377, 'columns');
        this.game.add.sprite(666, 377, 'columns');
        this.game.add.sprite(772, 377, 'columns');
       	this.game.add.sprite(865, 377, 'columns');
        this.game.add.sprite(971, 377, 'columns');
        //--Clock
        this.game.add.sprite(416, 177, 'clock');
        this.game.add.sprite(455, 194, 'clock-base');
        clock_time = this.check_the_clock();
        this.game.add.sprite(clock_time[3], clock_time[4], 'clockwise-minutes', clock_time[5]);
        this.game.add.sprite (clock_time[0], clock_time[1], 'clockwise-hour', clock_time[2]);
        
        //Interface HUD, Head-up display:
        score = 0;
        hp = 3;
        this.game.physics.arcade.isPaused = false;
        scoreString = '$: ';
        scoreText = this.game.add.text(10, 10, scoreString + score, {font: '34px Arial', fill: '#000'});

        hpString = 'hp: ';
        hpText = this.game.add.text(this.game.world.width - 100, 10, hpString + hp, {font: '34px Arial', fill: '#000'});
	},
    update: function(){ 
        //IA and others.
        this.game.physics.arcade.collide(enemies, platforms);

        enemies.forEach(this.checkWall, this, true);
        enemies.forEach(this.checkInside, this, true);

        scoreText.text = scoreString + score;

        if (hp > 0) {
            hpText.text = hpString + hp;
        } else {
            hpText.text = "DEAD";

            enemies.forEach(this.mock, this, true, scoreText);

            this.game.physics.arcade.isPaused = true;
        }
    },
    createEnemy: function() {
        if (!this.game.physics.arcade.isPaused) {
            var pos = positions[parseInt(Math.random() * 10)];

            var enemy = enemies.create(pos['x'], pos['y'], 'cowboy', 0);

            var walkRight = enemy.animations.add('left', [2, 3, 2, 4, 2, 3, 2, 4]);
            var walkLeft = enemy.animations.add('right', [5, 6, 5, 7, 5, 6, 5, 7]);
            var death = enemy.animations.add('death', [1]);
            var draw = enemy.animations.add('draw', [8, 9, 10, 10, 10, 11]);
            enemy.animations.add('mock', [12]);
            var snap = enemy.animations.add('snap', [0,13]);

            walkRight.onLoop.add(this.walkLooped, this);
            walkLeft.onLoop.add(this.walkLooped, this);

            walkRight.onComplete.add(this.stopWalking, this);
            walkLeft.onComplete.add(this.stopWalking, this);

            death.onComplete.add(this.destroyEnemy, this);

            draw.onComplete.add(this.enemyShoot, this);

            snap.onComplete.add(this.playerInjured, this);

            enemy.scale.set(2.5);
            enemy.smoothed = false;

            enemy.inputEnabled = true;
            //enemy.input.useHandCursor = true;
            enemy.events.onInputDown.add(this.killEnemy, this);

            this.game.physics.arcade.enable(enemy);
            enemy.body.gravity.y = 1000;
            enemy.body.collideWorldBounds = true;


            if (Math.random() >= 0.5) {
                enemy.body.velocity.x = enemySpeed;
                enemy.play('right', enemyFPS, true);
            } else {
                enemy.body.velocity.x = -enemySpeed;
                enemy.play('left', enemyFPS, true);
            }
        }
    },
    killEnemy: function(enemy) {
        if (!this.game.physics.arcade.isPaused) {
            score += scoreIncrement;

            enemy.body.velocity.x = 0;
            enemy.play('death', enemyFPS, false);

            switch(score) {
            case 500 :
                spawner.delay = 900;
                break;
            case 1000 :
                spawner.delay = 800;
                break;
            case 2000 :
                spawner.delay = 700;
                break;
            case 3500 :
                spawner.delay = 600;
                break;
            case 5000 :
                spawner.delay = 500;
                break;
            case 25000 :
                spawner.delay = 400;
                break;
            }
        }    
    },
    destroyEnemy: function(enemy) {
        enemy.destroy();
    },
    walkLooped: function(enemy, animation) {
        if (animation.loopCount > 1) {
            animation.loop = false;
        }
    },
    stopWalking: function(enemy, animation) {
        enemy.play('draw', enemyFPS, false);
        enemy.body.velocity.x = 0;
    },
    enemyShoot: function(enemy) {
        hp--;

        if (hp > 0) {
            enemy.play('snap', enemyFPS, false);
        }
    },
    playerInjured: function(enemy) {
        if (enemy.x >= this.game.world.width/2) {
            enemy.body.velocity.x = enemySpeed;
            enemy.play('right', enemyFPS, true);
        } else {
            enemy.body.velocity.x = -enemySpeed;
            enemy.play('left', enemyFPS, true);
        }
        enemy.body.collideWorldBounds = false;
    },
    checkWall: function(enemy) {
        if (enemy.body.onWall()) {
            if (enemy.x == 0) {
                enemy.play('right', enemyFPS, false);
                enemy.body.velocity.x = enemySpeed;
            } else {
                enemy.play('left', enemyFPS, false);
                enemy.body.velocity.x = -enemySpeed;
            }
        }
    },
    checkInside: function(enemy) {
        if (enemy.x > this.game.world.width || (enemy.x + enemy.width) < 0) {
            enemy.destroy();
        }
    },
    mock: function(enemy, scoreText) {
        enemy.body.velocity.x=0;
        enemy.play('mock', enemyFPS, false);

        scoreText.text = "";
        this.game.state.start("GameOver",false,false,score);
    },
    check_the_clock: function() {
        var hours_clockx='';
        var hours_clocky='';
        var hours_clockframe='';
        var hours = new Date().getHours();
        switch (hours){
            case 15 || 3: 
                        hours_clockx = 484;
                        hours_clocky = 228;
                        hours_clockframe = 9;
                        break;
            case 18 || 6: 
                        hours_clockx = 483;
                        hours_clocky = 238;
                        hours_clockframe = 3;
            case 21 || 9: 
                        hours_clockx = 473;
                        hours_clocky = 228;
                        hours_clockframe = 9;
                        break;
            case 24 || 12 || 0: 
                        hours_clockx = 483;
                        hours_clocky = 223;
                        hours_clockframe = 3;
                        break;
            default: 
                    if ((hours < 3 && hours > 1) ||  (hours < 15 && hours > 13)) {
                        hours_clockx = 489;
                        hours_clocky = 223;
                        hours_clockframe = 5;
                    } else if ((hours < 6 && hours > 3) ||  (hours < 18 && hours > 15)) {
                        hours_clockx = 489;
                        hours_clocky = 237;
                        hours_clockframe = 1;
                    } else if ((hours < 9 && hours > 6) ||  (hours < 21 || hours > 18)) {
                        hours_clockx = 477;
                        hours_clocky = 235;
                        hours_clockframe = 5;
                    } else if ((hours < 12 && hours > 8) ||  (hours < 24 && hours > 21)) {
                        hours_clockx = 478;
                        hours_clocky = 226;
                        hours_clockframe = 1;
                    } else {
                        hours_clockx = 489;
                        hours_clocky = 237;
                        hours_clockframe = 1;
                    }
                    break;
        }
        var minutes_clockx='';
        var minutes_clocky='';
        var minutes_clockframe='';
        var minutes = new Date().getMinutes();
        switch (minutes){
            case 0: 
                    minutes_clockx = 485;
                    minutes_clocky = 209;
                    minutes_clockframe = 2;
                    break;
            case 15: 
                    minutes_clockx = 493;
                    minutes_clocky = 222;
                    minutes_clockframe = 5;
                    break;
            case 30: 
                    minutes_clockx = 485;
                    minutes_clocky = 237;
                    minutes_clockframe = 2;
                    break;
            case 45: 
                    minutes_clockx = 464;
                    minutes_clocky = 222;
                    minutes_clockframe = 5;
                    break;
            default: 
                    if (minutes < 15) {
                        minutes_clockx = 496;
                        minutes_clocky = 211;
                        minutes_clockframe = 4;
                    } else if (minutes < 30 && minutes > 15) {
                        minutes_clockx = 495;
                        minutes_clocky = 235;
                        minutes_clockframe = 0;
                    }else if (minutes < 45 && minutes > 30) {
                        minutes_clockx = 477;
                        minutes_clocky = 230;
                        minutes_clockframe = 4;
                    } else if (minutes > 45) {
                        minutes_clockx = 476;
                        minutes_clocky = 216;
                        minutes_clockframe = 0;
                    } else {
                        minutes_clockx = 485;
                        minutes_clocky = 209;
                        minutes_clockframe = 2;
                    }
                    break;
        }
        var hours_and_minutes = [
            hours_clockx,
            hours_clocky,
            hours_clockframe,
            minutes_clockx,
            minutes_clocky,
            minutes_clockframe];
        return hours_and_minutes;
    }
}