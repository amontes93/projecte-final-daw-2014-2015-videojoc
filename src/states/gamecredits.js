var gameCredits = function(game){
	enableButtons = true;
}

gameCredits.prototype = {
  	create: function(){
  		var companyLogo = this.game.add.sprite(500,160,"spiraldawn_logo_circuit");
		companyLogo.scale.set(0.6);
		companyLogo.anchor.setTo(0.5,0.5);
		
		var authorText = this.game.add.text(500, 350, "Agustí Montes\n  Ricard Muns", {font: '24px Arial', fill: '#fff'});
  		authorText.anchor.setTo(0.5,0.5);
  		
  		var iesCopernicLogo = this.game.add.sprite(100, this.game.world.height - 50, "logo_ies_copernic");
  		iesCopernicLogo.anchor.setTo(0.5,0.5);
  		var iesCopernicText = this.game.add.text(300, this.game.world.height - 60, "Desenvolupament\nd'aplicacions web", {font: '24px Arial', fill: '#fff'});
  		iesCopernicText.anchor.setTo(0.5,0.5);
  		
  		var phaserText = this.game.add.text(this.game.world.width - 160, this.game.world.height - 60, "Desktop and mobile HTML5\n game framework", {font: '24px Arial', fill: '#fff'});
  		phaserText.anchor.setTo(0.5,0.5);
  		var phaserLogo = this.game.add.sprite(this.game.world.width - 380, this.game.world.height - 60, "img_phaser_logo");
  		phaserLogo.scale.set(0.2);
		phaserLogo.anchor.setTo(0.5,0.5);
		
		this.game.input.onDown.add(this.returnGameMenu,this);
  	},
	returnGameMenu: function(){
		this.game.state.start("Boot",true,true);
	},
 }