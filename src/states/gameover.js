var gameOver = function(game){
	finalScore = 0;
}

gameOver.prototype = {
	init: function(score){
		finalScore = score;
	},
  	create: function(){
  		var gameOverTitle = this.game.add.sprite(500,80,"poster_gameOver");
  		gameOverTitle.scale.set(0.5);
		gameOverTitle.anchor.setTo(0.5,0.5);

		var posterScore = this.game.add.sprite(500, 210, "poster_score");
		posterScore.scale.set(1.2);
		posterScore.anchor.setTo(0.5,0.5);

		var finalScoreText = this.game.add.text(600, 235, finalScore, {font: '34px Arial', fill: '#000'});
		if (finalScore == 0) {
			finalScoreText = this.game.add.text(600, 235, "0", {font: '34px Arial', fill: '#000'});
		}
		finalScoreText.anchor.setTo(1.0,1.0);
		var menuButton = this.game.add.button(500,320,"poster_main_menu",this.menuOfGame,this);
		menuButton.scale.set(0.4);
		menuButton.anchor.setTo(0.5,0.5);

		var rePlayButton = this.game.add.button(500,420,"poster_re_play",this.rePlayTheGame,this);
		rePlayButton.scale.set(0.4);
		rePlayButton.anchor.setTo(0.5,0.5);
	},
	menuOfGame: function(){
		//this.game.state.start("TheGame");
		this.game.state.start("Boot",true,true);
	},
	rePlayTheGame: function(){
		this.game.state.start("TheGame");
	}
}