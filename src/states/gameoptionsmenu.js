var gameOptionsMenu = function(game){
	enableButtons = true;
}

gameOptionsMenu.prototype = {
  	create: function(){
  		var comingText = this.game.add.text(500, 300, "Cooming Soon...", {font: '24px Arial', fill: '#fff'});
  		comingText.anchor.setTo(0.5,0.5);
  		this.game.input.onDown.add(this.returnGameMenu,this);
  	},
  	returnGameMenu: function(){
		this.game.state.start("Boot",true,true);
	}
  }