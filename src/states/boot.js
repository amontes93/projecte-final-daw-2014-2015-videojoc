var boot = function(game){
	console.log("%c %c %c %c %c %c %c %c %c %c Rail shooter Game | Agusti Montes i Ricard Muns |", "background:yellow", "background:red", "background:yellow", "background:red","background:yellow","background:red" ,"background:yellow","background:red" ,"background:yellow","background:white");
};
  
boot.prototype = {
	preload: function(){
        this.game.load.image("loading","src/assets/images/loading_bar_bullets3.png"); 
	},
  	create: function(){
		this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.scale.pageAlignHorizontally = true;
		this.scale.setScreenSize();
		this.game.stage.disableVisibilityChange = true;
		this.game.state.start("Preload");
	}
}